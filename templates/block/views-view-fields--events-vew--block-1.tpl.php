<?php

/**
* @file
* Default simple view template to all the fields as a row.
*
* - $view: The view in use.
* - $fields: an array of $field objects. Each one contains:
*   - $field->content: The output of the field.
*   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
*   - $field->class: The safe class id to use.
*   - $field->handler: The Views field handler object controlling this field. Do not use
*     var_export to dump this object, as it can't handle the recursion.
*   - $field->inline: Whether or not the field should be inline.
*   - $field->inline_html: either div or span based on the above flag.
*   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
*   - $field->wrapper_suffix: The closing tag for the wrapper.
*   - $field->separator: an optional separator that may appear before a field.
*   - $field->label: The wrap label text to use.
*   - $field->label_html: The full HTML of the label to use including
*     configured element type.
* - $row: The raw result object from the query, with all data it fetched.
*
* @ingroup views_templates
*/
//echo "<pre>";print_r($fields);die;

// dpm($row);

$photo = file_create_url($row->field_field_picture[0]['rendered']['#item']['uri']); 

//$photo = file_create_url($row->field_field_picture[0]['rendered']['#item']['uri']); 


?>




			
	
			
		
				<div class="col-md-4">
					<div class="event-post">
						<div class="thumb">
							<img src="<?php print $photo; ?>" alt="">
							<div class="overlay">
								<a href="#">Join Us</a>
								<a data-toggle="modal" href="#modal-donate-now">Donate Now</a>
							</div>
						</div>
						<div class="caption">
							<h3 class="title">
								<?php print $fields['title']->content; ?>
							</h3>
							<i class="fa fa-map-marker"></i>
							<p class="event-time"><span>
								<?php print $fields['field_date']->content; ?>
								
							</span></p>
							<p class="event-location"><?php print $fields['field_location']->content; ?></p>
						</div>
					</div>
				</div>
			
		
		