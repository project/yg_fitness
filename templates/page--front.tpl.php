
<!-- Navigation
    ==========================================-->
<header class="site-header">
     
      
      <nav id="site-nav" class="main-navigation navbar navbar-default navbar-fixed-top" role="navigation">


        <div class="container">
          <div class="row">
            <div class="col-sm-3">

                <?php if ($logo): ?>
                    <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" 
                        title="<?php print t('Home'); ?>">
                      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                    </a>
                  <?php endif; ?>
            
            </div>
            
            <div class="col-sm-9">

             <nav id="menu" class="navbar navbar-default navbar-top-fixed"> 
             <!-- <nav id="menu" class="navbar navbar-default navbar-fixed-top">    -->
                <div class="navigation">

                 <div class="container">
                    <div class="navbar-header menu-bar">
                      

                      <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                          <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                      <?php endif; ?>
                    </div>

                    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
                      <div class="navbar-collapse collapse" id="navbar-collapse">
                        <nav role="navigation">
                          <?php if (!empty($primary_nav)): ?>
                            <?php print render($primary_nav); ?>
                          <?php endif; ?>
                          
                        </nav>
                     
                    <?php endif; ?>
                  </div>
                 </div> 
                </div>
                </nav>



                            

            </div>
          </div>
        </div>
      </nav>
      <nav id="responsive-menu"></nav>
    </header>



<!-- Header -->
<header id="header">
  <?php print $slider; ?>

  <?php //print render($page['slider']); ?> 

</header>
<!-- About Section -->
<div id="about">
  <div class="container">  
   
        <?php print render($page['about']); ?>

  </div>
</div>
<!-- Porfolio Section -->
<div id="programs" class="text-center">
  <div class="container">
    
    
      <div class="programs-items">
            
        <?php print render($page['programs']);?>

      </div>
    
  </div>
</div>
<!-- Trainers Section -->
<div id="trainers" class="text-center">
  <div class="container">
   
        <?php print render($page['trainer']);?>


  </div>
</div>
<!-- Testimonials Section -->
<div id="testimonials">
  <div class="container">
    <div class="col-md-6 col-md-offset-6">
    
        <?php print render($page['testimonials']); ?>

    </div>
  </div>
</div>
<!-- Membership Section -->
<div id="membership" class="text-center">
  <div class="container">  
    <div id="pricing-table" class="clear">
      
    <?php print render($page['membership_pricing']); ?>

    </div>
  </div>
</div>
<!-- Contact Section -->
<div id="contact" class="text-center">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
       
            <?php print render($page['contact']) ?>

      </div>
    </div>
    <div class="col-md-12 contact-info">
      <div class="contact-item col-md-4">
        <p><span><i class="fa fa-map-marker"></i> Address</span><?php print theme_get_setting('address'); ?></p>
      </div>
      <div class="contact-item col-md-4">
        <p><span><i class="fa fa-phone"></i> Phone</span> <?php print theme_get_setting('phone'); ?> </p>
      </div>
      <div class="contact-item col-md-4">
        <p><span><i class="fa fa-envelope-o"></i> Email</span> 
            <?php 
               print theme_get_setting('email'); 
             ?>

        </p>
      </div>
    </div>
  </div>
</div>
<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <p><?php print render($page['copyright']) ?></p>
  </div>
</div>
