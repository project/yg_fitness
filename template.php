<?php

 // <div id="carousel-slide" class="carousel slide carousel-fade">
 //    <!-- Indicators -->
 //    <ol class="carousel-indicators">
 //        <li data-target="#carousel-slide" data-slide-to="0" class="active"></li>
 //        <li data-target="#carousel-slide" data-slide-to="1"></li>
 //        <li data-target="#carousel-slide" data-slide-to="2"></li>
 //        <li data-target="#carousel-slide" data-slide-to="3"></li>
 //    </ol>
 //    <!-- Wrapper for slides -->
 //    <div class="carousel-inner" role="listbox">
     
     
     
  
 //    </div>
 //    <!-- End Wrapper for slides-->
    
 //  </div>

function fitness_preprocess_page(&$variables){
$slider = "<div id='carousel-slide' class='carousel slide carousel-fade'>
            <ol class='carousel-indicators'>";
           for($i=0;$i<variable_get('number_of_slides', 1);$i++) {
               if($i==0) { $val='active'; } else { $val=''; }
                $slider .="<li data-target='#carousel-slide' data-slide-to='".$i."' class='".$val."'></li>";
           }
           $slider .="</ol><div class='carousel-inner' role='listbox'>";
           for($i=0;$i<variable_get('number_of_slides', 1);$i++) {
            if($i==0) { $val='active'; } else { $val=''; }
            $fid = theme_get_setting('slider_image'.$i);
            $slider_url = file_create_url(file_load($fid)->uri);
            $textname=theme_get_setting('textname'.$i);
            $description=theme_get_setting('slider_description'.$i);
            $link   =theme_get_setting('link'.$i);
            $slider .="<div class='item slide".$i." ".$val."''>
                        <div class='intro'>
                          <div class='overlay'>
                            <img class='img-responsive' src='".$slider_url."' style='margin: 0px auto' />
                            <div class='col-md-12 intro-text'>
                              <h1>".$textname."</h1>
                              <p>".$description."</p>
                              <a href='".$link."' class='btn btn-custom white btn-lg page-scroll'>Read More</a>
                            </div>
                          </div>
                        </div>
                      </div>";  
            }
            $slider .= "</div><a class='left carousel-control' href='#carousel-slide' role='button' data-slide='prev'>
                    <i class='glyphicon glyphicon-chevron-left'></i><span class='sr-only'>Previous</span>
                </a>
                <a class='right carousel-control' href='#carousel-slide' role='button' data-slide='next'>
                    <i class='glyphicon glyphicon-chevron-right'></i><span class='sr-only'>Next</span>
                </a>
            </div>";
            $variables['slider']= $slider;

}

 