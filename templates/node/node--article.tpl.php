



<div id="page-not-found">
  <div class="container">  
    <div class="row"> 
      <div class="col-xs-12 col-md-12 text-center">
        <div class="section-title">
          <h1>404</h1>
          <h2>Page Not Found</h2>
        </div>        
        <div class="page-not-found-text">
          <p>Sorry! The page you are trying to reach does not exist, or has been moved.</p>
          <a href="http://localhost/fitness" class="btn btn-custom btn-lg page-scroll">Back To Home</a>
        </div>
      </div>      
    </div>
  </div>
</div>