<?php








function fitness_form_system_theme_settings_alter(&$form, &$form_state) {



  $form['fitness'] = array(
      '#type' => 'fieldset',
      '#title' => t('Social Icons'),
      '#group' => 'bootstrap',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

   $form['fitness']['social_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social icons URL Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['fitness']['social_settings']['facebook'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#default_value' => theme_get_setting('facebook'),
    '#description' => t("Enter your Facebook Url."),
  );

$form['fitness']['social_settings']['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#default_value' => theme_get_setting('twitter'),
    '#description' => t("Enter your Twitter Url."),
  );


  $form['fitness']['social_settings']['google_plus'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Plus'),
    '#default_value' => theme_get_setting('google_plus'),
    '#description' => t("Enter your Google Plus Url."),
  );

   $form['fitness']['social_settings']['linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('Linkedin'),
    '#default_value' => theme_get_setting('linkedin'),
    '#description' => t("Enter your Linkedin Url."),
  );

  $form['fitness']['social_settings']['pinterest'] = array(
    '#type' => 'textfield',
    '#title' => t('Pinterest'),
    '#default_value' => theme_get_setting('pinterest'),
    '#description' => t("Enter your Pinterest Url."),

  );
  $form['fitness']['social_settings']['youtube'] = array(
    '#type' => 'textfield',
    '#title' => 'Youtube',
    '#default_value' => theme_get_setting('youtube'),
    '#description' => t('Enter your youtube url'),
  );






$form['fitness']['site_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site Information'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['fitness']['site_info']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Id'),
    '#default_value' => theme_get_setting('email'),
    '#description' => t("Please Enter your Email Id."),
  );

  $form['fitness']['site_info']['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Number'),
    '#default_value' => theme_get_setting('phone'),
    '#description' => t("Please Enter your Contact Number."),
  );   
  $form['fitness']['site_info']['address'] = array(
    '#type' => 'textarea',
    '#title' => t('Address'),
    '#default_value' => theme_get_setting('address'),
    '#description' => t("Please Enter your Address."),
  );   






// ..........................SLIDER...............

$form_state['build_info']['files'][] = drupal_get_path('theme', 'fitness') . '/slider_form/theme-settings.inc';
  $no_js_use = FALSE;
  
//$form['#tree'] = TRUE;
$form['fitness']['home_slider'] = array(
  '#type' => 'fieldset',
  '#title' => t('Slider'),
  '#group' => 'bootstrap',
  '#collapsible' => TRUE,
  '#collapsed' => FALSE,
);
$form['fitness']['home_slider']['names_fieldset'] = array(
  '#type' => 'markup',
  //'#title' => t('People coming to the picnic'),
  // Set up the wrapper so that AJAX will be able to replace the fieldset.
  '#prefix' => '<div id="names-fieldset-wrapper">',
  '#suffix' => '</div>',
);

// Build the fieldset with the proper number of names. We'll use
// $form_state['num_names'] to determine the number of textfields to build.
if (empty($form_state['num_names'])) {

  $form_state['num_names'] = variable_get('number_of_slides', 1);
}
//dpm(variable_get('number_of_slides', 1));
for ($i = 0; $i < $form_state['num_names']; $i++) {
  $form['fitness']['home_slider']['names_fieldset']['slide'.$i] = array(
    '#type' => 'fieldset',
    '#title' => t('Slide - '.$i),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="names-fieldset-wrapper">',
    '#suffix' => '</div>',
  );
 
  $form['fitness']['home_slider']['names_fieldset']['slide'.$i]['textname'.$i] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => theme_get_setting('textname'.$i),
  );
  
  $form['fitness']['home_slider']['names_fieldset']['slide'.$i]['slider_image'.$i] = array(
    '#type' => 'managed_file',
    '#title' => t('Image'),
    '#name' => 'files[]',
    '#upload_location' => 'public://',
    '#default_value' => theme_get_setting('slider_image'.$i),
    '#description' => t('Show a trail of links from the homepage to the current page.'),
    '#attributes' => array('multiple' => 'multiple'),
  );
  $form['fitness']['home_slider']['names_fieldset']['slide'.$i]['slider_description'.$i] = array(
    '#type' => 'textarea',
    '#title' => t('Slider Description'),
    '#description' => t('This text will be shown on the slider image.'),
    '#default_value' => theme_get_setting('slider_description'.$i),
  );
   $form['fitness']['home_slider']['names_fieldset']['slide'.$i]['link'.$i] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => theme_get_setting('link'.$i),
  );
  
}
$form['fitness']['home_slider']['names_fieldset']['slide'.$i]['add_name'] = array(
  '#type' => 'submit',
  '#value' => t('Add one more'),
  '#submit' => array('fitness_add_more_add_one'),
  // See the examples in ajax_example.module for more details on the
  // properties of #ajax.
  '#ajax' => array(
    'callback' => 'fitness_add_more_callback',
    'wrapper' => 'names-fieldset-wrapper',
  ),
);
if ($form_state['num_names'] > 1) {
  $form['fitness']['home_slider']['names_fieldset']['slide'.$i]['remove_name'] = array(
    '#type' => 'submit',
    '#value' => t('Remove one'),
    '#submit' => array('fitness_add_more_remove_one'),
    '#ajax' => array(
      'callback' => 'fitness_add_more_callback',
      'wrapper' => 'names-fieldset-wrapper',
    ),
  );
}
$form['#submit'][] = 'fitness_form_system_theme_settings_submit';

// This simply allows us to demonstrate no-javascript use without
// actually turning off javascript in the browser. Removing the #ajax
// element turns off AJAX behaviors on that element and as a result
// ajax.js doesn't get loaded.
// For demonstration only! You don't need this.
if ($no_js_use) {
  // Remove the #ajax from the above, so ajax.js won't be loaded.
  if (!empty($form['names_fieldset']['remove_name']['#ajax'])) {
    unset($form['names_fieldset']['remove_name']['#ajax']);
  }
  unset($form['names_fieldset']['add_name']['#ajax']);
}
variable_set('number_of_slides', $form_state['num_names']);







}