<?php

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function fitness_add_more_callback($form, $form_state) {
  return $form['fitness']['home_slider']['names_fieldset'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function fitness_add_more_add_one($form, &$form_state) {
  $form_state['num_names']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function fitness_add_more_remove_one($form, &$form_state) {
  if ($form_state['num_names'] > 1) {
    $form_state['num_names']--;
  }
  $form_state['rebuild'] = TRUE;
}


function fitness_form_system_theme_settings_submit($form, &$form_state) {

  for($i=0;$i<=10;$i++){
  if(!empty($form_state['values']['slider_image'.$i])){
    $fid = $form_state['values']['slider_image'.$i];
      $file = file_load($fid);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'custom_status', 'slide_picture', $file->fid);
    }
  }
 
}
